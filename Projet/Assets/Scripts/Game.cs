﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public static MapManager map { get; private set; }
    public static UserInterface ui { get; private set; }
    public static PlayerController player { get; private set; }
    public static CameraController camera { get; private set; }
    public static InputManager input { get; private set; }
    public static SaveManager save { get; private set; }
    //public static BattleManager battle { get; private set; }
    //public static Fighter[] allies { get; private set; }


    private void Awake()
    {
        map = FindObjectOfType<MapManager>();
        ui = FindObjectOfType<UserInterface>();
        player = FindObjectOfType<PlayerController>();
        camera = FindObjectOfType<CameraController>();
        input = FindObjectOfType<InputManager>();
        save = FindObjectOfType<SaveManager>();
        //battle = FindObjectOfType<BattleManager>();
    }

    // Use this for initialization
    void Start()
    {
        //allies = new Fighter[1];
        //allies[0] = player.GetComponent<Fighter>();
        //map.LoadScene("map1", "default");
    }
}
