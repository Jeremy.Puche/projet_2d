﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class SpriteAnimation
{
    public string m_Id;

    [SerializeField]
    private Sprite[] m_Sprites;
    [SerializeField]
    private float m_FrameDuration = 1.0f;

    [HideInInspector]
    public SpriteRenderer m_Renderer;


    private int m_SpriteIndex = 0;
    private float m_FrameRemainingTime = 0.0f;

    public void Reset()
    {
        m_SpriteIndex = 0;
        m_FrameRemainingTime = 0;
    }

    // Update is called once per frame
    public void DoUpdate()
    {
        if (m_FrameRemainingTime <= 0.0f)
        {


            if (m_Sprites.Length > 0)
            {

                m_Renderer.sprite = m_Sprites[m_SpriteIndex];
                m_SpriteIndex = (m_SpriteIndex + 1) % m_Sprites.Length;
            }
            m_FrameRemainingTime = m_FrameDuration;
        }
        m_FrameRemainingTime -= Time.deltaTime;
    }

}
