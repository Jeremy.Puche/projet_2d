﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UserInterface : MonoBehaviour
{
    public ScreenFader fader { get; private set; }

    [SerializeField]
    private RectTransform m_DialogueBox;
    [SerializeField]
    private TextMeshProUGUI m_DialogueText;


    private void Awake()
    {
        fader = GetComponentInChildren<ScreenFader>();
    }

    public void DisplayDialogueBox(string msg)
    {
        m_DialogueBox.gameObject.SetActive(true);
        m_DialogueText.SetText(msg);
    }

    public void HideDialogBox()
    {
        m_DialogueBox.gameObject.SetActive(false);
    }
}
