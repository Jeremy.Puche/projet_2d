﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;

public class SaveManager : MonoBehaviour
{
    private string SAVE_FILE_PATH;

    private void Awake()
    {
        SAVE_FILE_PATH = Application.persistentDataPath + "/save.json";
        Debug.Log(SAVE_FILE_PATH);
    }

    public void SaveGame()
    {
        SaveData data = new SaveManager.SaveData
        {
            characterPositionX = Game.player.transform.position.x,
            characterPositionY = Game.player.transform.position.y,
            currentMap = Game.map.CurrentMap
        };

        string json = JsonConvert.SerializeObject(data);
        File.WriteAllText(SAVE_FILE_PATH, json);
    }

    public void LoadGame()
    {
        if (!File.Exists(SAVE_FILE_PATH))
        {
            Debug.LogWarning("The game was never saved");
            return;
        }

        string json = File.ReadAllText(SAVE_FILE_PATH);
        SaveData data = JsonConvert.DeserializeObject<SaveData>(json);

        Game.map.LoadScene(data.currentMap, new Vector3(data.characterPositionX, data.characterPositionY));
    }

    public class SaveData
    {
        public float characterPositionX;
        public float characterPositionY;
        public string currentMap;
    }
}
