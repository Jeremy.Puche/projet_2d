﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ScreenFader : MonoBehaviour
{

    [Range(0.0f, 1.0f)]
    public float m_FadeRatio = 1.0f;
    private Image m_Image;
    public bool Fading = false;

    public event Action onFadeComplete;

    // Use this for initialization
    void Start()
    {
        m_Image = GetComponent<Image>();
        StartFadeOut();
    }

    // Update is called once per frame
    void Update()
    {
        m_Image.color = new Color(0, 0, 0, m_FadeRatio);
    }

    public void StartFadeIn()
    {
        if (!Fading)
        {
            Fading = true;
            StartCoroutine(FadeIn());
        }
    }

    IEnumerator FadeIn()
    {
        Fading = true;
        while (m_FadeRatio < 1.0f)
        {
            m_FadeRatio += Time.deltaTime * 0.5f;
            yield return null;
        }
        Fading = false;

        if (onFadeComplete != null)
            onFadeComplete();
    }


    public void StartFadeOut()
    {
        if (!Fading)
        {
            Fading = true;
            StartCoroutine(FadeOut());
        }
    }

    IEnumerator FadeOut()
    {
        Fading = true;
        while (m_FadeRatio > 0.0f)
        {
            m_FadeRatio -= Time.deltaTime * 0.5f;
            yield return null;
        }
        Fading = false;

        if (onFadeComplete != null)
            onFadeComplete();
    }
}
